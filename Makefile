LIB = libnativewsk.a
OBJ = nativewsk.o

CFLAGS = -O0 -g
CFLAGS += -Wall -Werror -pedantic

.PHONY: clean

$(LIB): $(OBJ)
	$(AR) rcs $@ $^

clean:
	rm -f $(LIB) $(OBJ)
