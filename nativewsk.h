#pragma once

#include "cJSON.h"

#include <stdbool.h>

/* OpenWhisk action interface for native actions:
 * * invocation parameters passed as a JSON string on stdin AND as first argument
 *   * always fits in MAX_ARG_STRLEN bytes
 * * function result as the last line on stdout
 *   * if it contains the key "error", then the activation counts as an application error
 *     * all other kays than "error" are then ignored
 */

/* Maximum size of the output. Defined by OpenWhisk's limit on result JSON string: 1MB.
 */
#define OUTPUT_MAX_SZ             (1024 * 1024)
/* Maximum size of binary data to be encoded as base64 and included in the action's result.
 *
 * This is not totally accurate because it does not account for other characters needed by the JSON format, not any
 * other data that may be included in the result.
 */
#define BINARY_OUTPUT_MAX_SZ      ((OUTPUT_MAX_SZ + 3) / 4 * 3)

/* Output function result.
 *
 * Write on stdout the function result (a JSON object), as a JSON string on a single line.
 *
 * If data is a JSON object, print it as-is;
 * otherwise, wrap it in a JSON object under the key "data".
 */
void output(const char *const msg, cJSON *const data);

/* Output function error.
 *
 * Write on stdout the function error (a JSON object), as a JSON string on a single line.
 *
 * The object has two keys:
 *
 * 1. "msg": the provided message
 *    * msg may be NULL, in which case the key "msg" is not present.
 * 2. "code": the error code
 */
void output_err(const char *msg, int code);

/* Create and add binary data to an object.
 *
 * The data is encoded in base64 and added as a String to the object.
 */
cJSON *cJSON_AddBinaryToObject(cJSON * const object, const char* const name, const char* const data, size_t data_sz);

/* Get an argument from the action's arguments.
 *
 * The name lookup is case sensitive.
 */
static inline cJSON *getarg(const cJSON *const args, const char *const name) {
  return cJSON_GetObjectItemCaseSensitive(args, name);
}

/* Get the int value of an argument.
 *
 * The int value will be stored at val.
 *
 * Returns -1 if arg is not a JSON Number;
 * otherwise returns 0.
 */
int getarg_as_int(const cJSON *const args, const char *const name, int *const val);

/* Get the boolean value of an argument.
 *
 * Returns -1 if arg is not a JSON Boolean;
 * otherwise returns 0.
 */
int getarg_as_bool(const cJSON *const args, const char *const name, bool *const val);

/* Get the string value of an argument.
 *
 * Returns -1 if arg is not a JSON String, or the String is null;
 * otherwise returns 0.
 */
int getarg_as_string(const cJSON *const args, const char *const name, char **val);

/* Get the binary value of a base64-encoded String argument.
 *
 * Returns -1 if arg is not a JSON String, or the String is null;
 * otherwise returns the length of the decoded binary.
 */
int getarg_as_binary(const cJSON *const args, const char *const name, char **val);

/* Get an argument as a JSON object.
 *
 * Returns -1 if arg is not a JSON Object;
 * otherwise returns 0.
 */
int getarg_as_object(const cJSON *const args, const char *const name, const cJSON **val);

/* Print function result on a single line on stdout.
 */
void print_return(const cJSON *const ret);
