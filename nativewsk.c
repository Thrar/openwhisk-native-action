#include "nativewsk.h"

#include "base64.h"
#include "cJSON.h"

#include <sys/mman.h>
#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void print_return(const cJSON *const ret) {
  char *res_print = cJSON_PrintUnformatted(ret);
  printf("%s", res_print);
  free(res_print);
}

void output(const char *const msg, cJSON *const data) {
  cJSON *ret = cJSON_CreateObject();

  if (msg)
    cJSON_AddStringToObject(ret, "msg", msg);
  if (data)
    cJSON_AddItemReferenceToObject(ret, "data", data);

  print_return(ret);

  cJSON_Delete(ret);
}

void output_err(const char *msg, int code) {
  cJSON *ret = cJSON_CreateObject();

  if (msg)
    cJSON_AddStringToObject(ret, "msg", msg);
  cJSON_AddNumberToObject(ret, "code", code);

  print_return(ret);

  cJSON_Delete(ret);
}

cJSON *cJSON_AddBinaryToObject(cJSON *const object, const char *const name,
                               const char *const data, size_t data_sz) {
  char *data_b64 = malloc(sizeof(char) * Base64encode_len(data_sz));
  cJSON *ret;

  Base64encode(data_b64, data, data_sz);

  ret = cJSON_AddStringToObject(object, name, data_b64);

  free(data_b64);

  return ret;
}

int getarg_as_int(const cJSON *const args, const char *const name,
                  int *const val) {
  cJSON *arg;

  if (!(arg = getarg(args, name)))
    return -1;

  if (!cJSON_IsNumber(arg))
    return -2;

  *val = arg->valueint;

  return 0;
}

int getarg_as_bool(const cJSON *const args, const char *const name,
                   bool *const val) {
  cJSON *arg;

  if (!(arg = getarg(args, name)))
    return -1;

  if (!cJSON_IsBool(arg))
    return -2;

  *val = cJSON_IsTrue(arg);

  return 0;
}

int getarg_as_string(const cJSON *const args, const char *const name,
                     char **val) {
  cJSON *arg;

  if (!(arg = getarg(args, name)))
    return -1;

  if (!cJSON_IsString(arg) || arg->valuestring == NULL)
    return -2;

  *val = arg->valuestring;

  return 0;
}

int getarg_as_binary(const cJSON *const args, const char *const name,
                     char **val) {
  int ret;
  char *data_b64;

  if ((ret = getarg_as_string(args, name, &data_b64)) < 0)
    return ret;

  *val = malloc(sizeof(char) * Base64decode_len(data_b64));

  return Base64decode(*val, data_b64);
}

int getarg_as_object(const cJSON *const args, const char *const name,
                     const cJSON **val) {
  cJSON *arg;

  if (!(arg = getarg(args, name)))
    return -1;

  if (!cJSON_IsObject(arg))
    return -2;

  *val = arg;

  return 0;
}
